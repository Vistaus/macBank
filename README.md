**What is a MAC address OUI?**

An OUI or Organizationally Unique Identifier is
the first 24 bits out of an 48 bit MAC address
which uniquely identifies a vendor, manufacturer,
or other organization.

**Usage**

Enter any MAC address, OUI or IAB and find its vendor or manufacturer.

**License**

Copyright (C) 2018 Aitzol Berasategi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
License version 3, as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public 
License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>