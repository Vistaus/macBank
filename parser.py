# Copyright 2018 Wproject - Aitzol Berasategi - https://aitzol.eus
#
# This file is part of macBank Ver.1.0.0.
#
# This project is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
#
# PyCalculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or ANY OTHER COMMERCIAL PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from io import open
import json

def file_parser(format, file):

    string = ""
    if format=="md":

        with open(file, 'r') as myfile:
            #data = myfile.read()
            line=myfile.readlines()
            myfile.close()
        for i in range(len(line)):
            if (line[i][0] == "*") and (line[i][-2] == "*"):
                titleLine = "\n<span size='x-large'>"+line[i][2:][:-3]+"</span>\n"
                string=string+titleLine

            elif (line[i][0]!="\n") and (line[i][0]!=" "):
                regularLine = line[i]
                string = string + regularLine

        return string

    elif format=="json":

        with open(file) as f:
            jsonData = json.load(f)

            string = "<span foreground='blue' size='x-large'>" + jsonData['name'] + \
                       "</span>\n\nver: " + jsonData['version'] + \
                       "\nLicense: " + jsonData['license'] + "\n" + jsonData['author']

            return string

def manifest(key):

    with open("manifest.json") as f:
        jsonData = json.load(f)

        value = jsonData[key]
        return value
